#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include "matrix.h"
#include "vector.h"

#define SIZE 200
#define DIM 16
#define MAX_THREAD 8

int main (void)
{
	srand (time(NULL));

	char option = '1';
		
	printf("To check vector multiplication press '1', or '2' to check matrix multipliaction.\n");

	option = getchar();

	if (option == '1')
	{
		struct Matrix A;
		struct Vector v;
		struct Vector w;
	
		A.rows = DIM; A.columns=DIM; A.m = malloc(sizeof(int)*A.rows*A.columns);
		v.rows = DIM; v.v = malloc(sizeof(int)*v.rows);
		w.rows = DIM; w.v = malloc(sizeof(int)*w.rows);

		int i=0;

		for (i=0; i < A.rows*A.columns; ++i)
			A.m[i] = (((double)rand()/(double)RAND_MAX) * SIZE);

		for (i=0; i < v.rows; ++i)
			v.v[i] = (((double)rand()/(double)RAND_MAX) * SIZE);

		for(i=0; i < w.rows; i++)
			w.v[i]=0;

		struct mv args[MAX_THREAD];
		pthread_t thread[MAX_THREAD];
		int rc;

		for (i=0; i < MAX_THREAD; i++)
		{
			args[i].A = A;
			args[i].v = v;
			args[i].w = &w;
			args[i].st = i;
			args[i].in = MAX_THREAD;
		}

		for (i=0; i < MAX_THREAD; i++)
		{
			rc = pthread_create(&thread[i], NULL, Mult_MV, (void *)&args[i]); 
			if (rc)
			{
				printf("ERROR: return code from pthread is %d\n", rc);
				exit(-1);
			}
		}

		for (i=0; i < MAX_THREAD; i++)
			pthread_join(thread[i], NULL);

		printf("Multiplication Complete\n");
	}

	else if (option == '2')
	{
		struct Matrix A;
		struct Matrix B;
		struct Matrix C;

		A.rows = DIM; A.columns=DIM; A.m = malloc(sizeof(int)*A.rows*A.columns);
		B.rows = DIM; B.columns=DIM; B.m = malloc(sizeof(int)*B.rows*B.columns);
		C.rows = DIM; C.columns=DIM; C.m = malloc(sizeof(int)*C.rows*C.columns);

		int i=0;

		for (i=0; i < A.rows*A.columns; ++i)
			A.m[i] = (((double)rand()/(double)RAND_MAX) * SIZE);

		for (i=0; i < B.rows*B.columns; ++i)
			B.m[i] = (((double)rand()/(double)RAND_MAX) * SIZE);

		for(i=0; i < C.rows*C.columns; i++)
			C.m[i]=0;

		struct mm args[MAX_THREAD];
		pthread_t thread[MAX_THREAD];
		int rc;

		for (i=0; i < MAX_THREAD; i++)
		{
			args[i].A = A;
			args[i].B = B;
			args[i].C = &C;
			args[i].st = i;
			args[i].in = MAX_THREAD;
		}
	
		for (i=0; i < MAX_THREAD; i++)
		{
			rc = pthread_create(&thread[i], NULL, Mult_MM, (void *)&args[i]); 
			if (rc)
			{
				printf("ERROR: return code from pthread is %d\n", rc);
				exit(-1);
			}
		}

		for (i=0; i < MAX_THREAD; i++)
			pthread_join(thread[i], NULL);

		
		printf("Multiplication Complete\n");
	}

	else
		printf("ERROR: Choice Invalid\n");

	return 0;
}
