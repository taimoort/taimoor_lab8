matrix : main.o vector.o matrix.o
	gcc -o matrix main.o vector.o matrix.o -pthread
main.o : main.c 
	gcc -c main.c -pthread

vector.o : vector.c vector.h
	gcc -c vector.c -pthread

matrix.o : matrix.c matrix.h
	gcc -c matrix.c -pthread

     clean :
	rm matrix.o vector.o Main.o
