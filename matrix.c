#include "matrix.h"

void Mult_MM (void *args)
{
	int i,j,k;
	struct mm* data = (struct data*)args;

	if (data->A.columns != data->B.rows)
	{
		printf("Columns of first matrix are not equal to the rows of the second matrix, so multiplication cannot be done.\n");
		getchar();
		exit(1);
	}

	for ( i=data->st; i<data->A.rows; i+=data->in)
		for( j=0; j<data->B.columns; ++j)
			for( k=0; k<data->A.columns; ++k)
				data->C->m[i*data->C->columns + j] += data->A.m[i*data->A.columns+k] * data->B.m[k*data->B.columns+j];

}
