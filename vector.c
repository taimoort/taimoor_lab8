#include "matrix.h"
#include "vector.h"

void Mult_MV (void *args)
{
	int i,j;
	struct mv* data = (struct data*)args;

	if (data->A.columns != data->v.rows)
	{
		printf("Columns of the matrix are not equal to the rows of the vector, so multiplication cannot be done.");
		exit(1);
	}

	for( i=data->st; i<data->A.rows; i+=data->in)
		for( j=0; j<data->A.columns; ++j)
			data->w->v[i] += data->A.m[i*data->A.columns+j] * data->v.v[j];			

}
